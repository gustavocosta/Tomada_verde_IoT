import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

from datetime import datetime

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, decimal.Decimal):
			if o % 1 > 0:
				return float(o)
			else:
				return int(o)
		return super(DecimalEncoder, self).default(o)

def bubbleSort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if alist[i]['time']<alist[i+1]['time']:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp

dynamodb = boto3.resource("dynamodb")

table = dynamodb.Table('PlugTable')

try:
	date = datetime.now()
	response = table.scan(FilterExpression=Key("device").eq("test"))
except ClientError as e:
	print(e.response['Error']['Message'])
else:
	for i in response['Items']:
		print(i['date'])
	bubbleSort(response['Items'])
	for i in response['Items']:
		print(i['date'])
		print(i['con_price'])

#include "EmonLib.h"                  

//Sensor
EnergyMonitor emon1;                   
double delta=0.00;
#define WINDOW 30
#define TEMPO 3600
const int sensorPin = 0;
int contador =0;
double valor=0;

void setup()
{  
  emon1.current(1, 111.1);            
  Serial.begin(9600);
  
  double vIrms[WINDOW];
  // Calibrate (find offset)
  double cIrms = 0;
  
  for (int i=0; i<WINDOW; i++) {
        //if (i>1){
        vIrms[i] = emon1.calcIrms(1480);
        cIrms = cIrms + vIrms[i];
        delay(100);
       // }
     }
      delta = cIrms/(WINDOW);
      double zero = 0.0000;
      Serial.print(zero, 4); 
      Serial.flush();   
      //Serial.print("Delta ");
      //Serial.println(delta);

  
}

void loop()
{
 while(contador <TEMPO)
 {
  unsigned long timethen = millis();
  double Irms = emon1.calcIrms(1480)-delta;
  unsigned long atraso = millis() - timethen;
  
  if(Irms <0.00){
    Irms=0.0000;
  }
  /*Serial.print("IRMS: ");*/
  //Serial.println(Irms);
  //Serial.flush(); */
 valor = valor +(Irms*210.0000);
  /*Serial.print("Valor: ");*/
 // Serial.println(valor);
  // Serial.flush(); */

   contador++;
   delay(1000-atraso);
  }
  
  double final = ((valor/TEMPO)/1000.0000);
  valor =0;  contador = 0;
  Serial.print(final,4); 
  Serial.flush(); 
}

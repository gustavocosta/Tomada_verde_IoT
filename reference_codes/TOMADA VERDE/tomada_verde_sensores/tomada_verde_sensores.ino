//#include <Thermistor.h>
#include <Wire.h>
#include <DS3231.h>

DS3231 clock;
RTCDateTime dt;

//Thermistor temp(0);

int segundo,minuto, hora, dia, mes,ano, diadasemana;
byte power =2;
byte cooler = 3;
void setup()
{  pinMode(power, OUTPUT);
   pinMode(cooler, OUTPUT);
     
    Serial.begin(9600);  
    
    clock.begin();

  // Set sketch compiling time
  //clock.setDateTime(__DATE__, __TIME__);
}

void loop()
{  
  dt = clock.getDateTime();
  
  ano = dt.year;
  mes = dt.month;
  dia = dt.day;
  hora = dt.hour;
  minuto = dt.minute;
  segundo = dt.second;
  diadasemana = dt.dayOfWeek;
  int horario = (hora*100) + minuto;
  //Serial.print("Raw data: ");
  Serial.print(ano);   Serial.print("-");
  Serial.print(mes);  Serial.print("-");
  Serial.print(dia);    Serial.print(" ");
  Serial.print(hora);   Serial.print(":");
  Serial.print(minuto); Serial.print(":");
  Serial.print(segundo); Serial.print(" ");
  Serial.print(diadasemana); Serial.println(" ");
  //DESLIGA DOMINGO
  if (diadasemana == 7)
  {
    digitalWrite(power, LOW); 
    digitalWrite(cooler, LOW); 
  }
  //LIGA NO SÁBADO ATÉ MEIO-DIA
  else if (diadasemana == 6)
  {
    if(hora >=8 && hora < 12){
      digitalWrite(power, HIGH); 
      if (minuto >= 20 && minuto <=40){
        digitalWrite(cooler, HIGH);
      }
      else{
        digitalWrite(cooler, LOW);
      }
    }
    else{
      digitalWrite(power, LOW);
      digitalWrite(cooler, LOW);
    }
    
  }
  //DIA DA SEMANA
  
  else{
    Serial.println(horario);
    if(horario >=730 && horario <1830){
      digitalWrite(power, HIGH); 
      if (minuto >= 1 && minuto <=20){
        digitalWrite(cooler, HIGH);
      }
      else{
        digitalWrite(cooler, LOW);
      }
    }
    else{
      digitalWrite(power, LOW);
      digitalWrite(cooler, LOW);
    }
    
  }
  
delay(1000);
 
}

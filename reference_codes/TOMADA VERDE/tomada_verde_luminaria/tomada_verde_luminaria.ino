#include <Wire.h>
#include <DS3231.h>

DS3231 clock;
RTCDateTime dt;


int segundo,minuto, hora, dia, mes,ano, diadasemana;
byte power =2;
byte light = 7;
void tempo(int diadasemana, int horario);

void setup()
{  pinMode(power, OUTPUT);
   pinMode(light, OUTPUT);
     
    Serial.begin(9600);  
    
    clock.begin();

  //Set sketch compiling time
clock.setDateTime(__DATE__, __TIME__);
}

void loop()
{  
  dt = clock.getDateTime();
  
 
  
  ano = dt.year;
  mes = dt.month;
  dia = dt.day;
  hora = dt.hour;
  minuto = dt.minute;
  segundo = dt.second;
  diadasemana = dt.dayOfWeek;
  int horario = (hora*100) + minuto;
  //Serial.print("Raw data: ");
  Serial.print(ano);   Serial.print("-");
  Serial.print(mes);  Serial.print("-");
  Serial.print(dia);    Serial.print(" ");
  Serial.print(hora);   Serial.print(":");
  Serial.print(minuto); Serial.print(":");
  Serial.print(segundo); Serial.print(" ");
 Serial.print(diadasemana); Serial.println(" ");
  Serial.println(horario);

  //DESLIGA DOMINGO
  if (diadasemana == 7)
  {
    
    if (horario >= 1800 && horario <=2200){
        digitalWrite(light, HIGH);
      }
    else{
      
      digitalWrite(light, LOW);
    }
    digitalWrite(power, LOW); 
    
  
  }
  //LIGA NO SÁBADO ATÉ ONZE
  else if (diadasemana == 6)
  {
    if(horario >=900 && horario <=1100){
      digitalWrite(power, HIGH);  
    }
    else{
      digitalWrite(power, LOW);
    }
    if (horario >= 1800 && horario <=2200){
        digitalWrite(light, HIGH);
      }
    else{
      
      digitalWrite(light, LOW);
    }
    
  }
  //DIA DA SEMANA
  
  else{
    if(horario >=800 && horario <=1759){
      digitalWrite(power, HIGH); 
    }
    else {
       digitalWrite(power, LOW);     
    }
    
     if (horario >= 1801 && horario <=2200){
        digitalWrite(light, HIGH);
     }  
    else{
      digitalWrite(light, LOW);
    }
    
  }
  
  if (digitalRead(light) == HIGH){
      Serial.println("LIGHT ON");
  }
  else{
  Serial.println("LIGHT OFF");
  }
  if (digitalRead(power) == HIGH){
      Serial.println("POWER ON");
  }
  else{
  Serial.println("POWER OFF");
  }
    
delay(1000);
}


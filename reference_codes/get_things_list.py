import boto3

client = boto3.client('iot')
request = client.list_things()

try:
	things_list = request['things']
	for thing in things_list:
		print(thing['thingName'])
except:
	print('Error on get things names')
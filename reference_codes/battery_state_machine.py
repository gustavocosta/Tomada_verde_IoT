state = 3; # IDLE = 0, FAULT = 1, DONE = 2, PRECHARGE = 3, CHARGE = 4, FLOAT = 5 
state_count = 600;

cells = 12;
CUTOFF_VOLTAGE = cells * 1.75
IMIN_UPDATE = 5
IFLAT_COUNT = 600
ILIM = 6

imin = ILIM
imin_db = IMIN_UPDATE
iflat_db = IFLAT_COUNT

while 1:
	voltage = float(input())
	current = float(input())

	if state == 3:
		if voltage < CUTOFF_VOLTAGE:
			if state_count:
				state_count = state_count -1
			else:
				state = 1
		else:
			state = 4
	else:

		if state == 4:
			if current < imin:
				if imin_db:
					imin_db = imin_db -1
				else:
					imin = current
					imin_db = IMIN_UPDATE
					iflat_db = IFLAT_COUNT
			else:
				imin_db = IMIN_UPDATE
				if iflat_db:
					iflat_db = iflat_db -1
		else:
			imin_db = IMIN_UPDATE
			iflat_db = IFLAT_COUNT
			imin = ILIM
		if(imin < ISTOP || !iflat_db)
		{
			#ifdef	BATTERY_SLA
				battery_state = FLOAT;
				state_counter = FLOAT_TIME;

				SET_VOLTAGE(FLOATING_VOLTAGE);
			#else
				battery_state = DONE;
				if(imin < I_BAT_DETECT) battery_state = IDLE;
			#endif
		}

	print(state)
console.log('Loading function');
var AWS = require('aws-sdk');
var dynamo = new AWS.DynamoDB.DocumentClient();
var table = "PlugTable";
var d = new Date();

exports.handler = function(event, context) {
	console.log('Received event:', JSON.stringify(event, null, 2));
	var params = {
		TableName:table,
		Item:{
			"device": event.state.reported.device,
			"date": d.getDate(),
			"month": d.getMonth(),
			"year": d.getYear(),
			"power": event.state.reported.power,
			"con_current": event.state.reported.con_current,
			"con_potency": event.state.reported.con_potency,
			"con_price": event.state.reported.con_price,
			"bat_current": event.state.reported.bat_current,
			"bat_voltage": event.state.reported.bat_voltage,
			"bat_charge": event.state.reported.bat_charge,
			"bat_state": event.state.reported.bat_state
		}
	};

	console.log("Adding a new IoT device...");
	dynamo.put(params, function(err, data) {
		if (err) {
			console.error("Unable to add device. Error JSON:", JSON.stringify(err, null, 2));
			//context.fail();
		} else {
			console.log("Added device:", JSON.stringify(data, null, 2));
			//context.succeed();
		}
	});
}
class battery_charge():
	def __init__(self):
		self.capacity = 100.0
		self.capacity_max = 7.0
		self.time = 1.0/60.0

		self.state = 3 # IDLE = 0, FAULT = 1, DONE = 2, PRECHARGE = 3, CHARGE = 4, FLOAT = 5
		self.state_count = 600;

		self.cells = 6
		self.CHARGING_VOLTAGE = self.cells * 2.40
		self.FLOATING_VOLTAGE = self.cells * 2.25
		self.CUTOFF_VOLTAGE = self.cells * 1.75
		self.TOPPING_VOLTAGE = self.cells * 2.10
		self.IMIN_UPDATE = 5
		self.IFLAT_COUNT = 600
		self.ILIM = 6
		self.IFLOAT = self.cells * 0.1
		self.FLOAT_TIME = 14400
		self.FLOAT_RELAX_TIME = 60
		self.I_BAT_DETECT = 16
		self.V_BAT_DETECT = 1640
		self.CURRENT_MODE = 4

		self.iref = self.ILIM
		self.vref = self.CHARGING_VOLTAGE
		self.imin = self.ILIM
		self.imin_db = self.IMIN_UPDATE
		self.iflat_db = self.IFLAT_COUNT
		self.cmode = 0
		self.cc_cv = self.CURRENT_MODE

	def b_charge(self, current):
		self.capacity = self.capacity + (current/self.capacity_max)*self.time * 100
		if(self.capacity > 100):
			self.capacity = 100
		if(self.capacity < 0):
			self.capacity = 0
		return self.capacity

	def b_state(self, current, voltage):
		if self.state == 3:
			if voltage < self.CUTOFF_VOLTAGE:
				if self.state_count:
					self.state_count = self.state_count -1
				else:
					self.state = 1
					self.capacity = 0
			else:
				self.state = 4
				self.capacity = 10
				self.iref = self.ILIM
		elif self.state == 4:
			if ~self.cmode:
				if current < self.imin:
					if self.imin_db:
						self.imin_db = self.imin_db -1
					else:
						self.imin = current
						self.imin_db = self.IMIN_UPDATE
						self.iflat_db = self.IFLAT_COUNT
				else:
					self.imin_db = self.IMIN_UPDATE
					if self.iflat_db:
						self.iflat_db = self.iflat_db -1
			else:
				self.imin_db = self.IMIN_UPDATE
				self.iflat_db = self.IFLAT_COUNT
				self.imin = self.ILIM
			if(self.imin < self.IFLOAT) | (~self.iflat_db):
				self.state = 5
				self.capacity = 50.0
				self.state_count = self.FLOAT_TIME
				self.iref = self.FLOATING_VOLTAGE
		elif self.state == 5:
			if self.state_count:
				self.state_count = self.state_count -1
			else:
				self.state = 2
				self.capacity = 100.0
			if(self.state_count < self.FLOAT_RELAX_TIME) & (current < self.I_BAT_DETECT):
				self.state = 0
				self.capacity = 0
		elif (self.state == 0) | (self.state == 1):
			self.iref = 0
			self.vref = 0
		elif (self.state == 2):
			if (voltage < self.TOPPING_VOLTAGE) & (voltage > self.V_BAT_DETECT):
				self.state = 4
				self.capacity = 10
				self.iref = self.ILIM
				self.vref = self.CHARGING_VOLTAGE
				self.imin = self.ILIM
				self.imin_db = self.IMIN_UPDATE
				self.iflat_db = self.IFLAT_COUNT
			else:
				self.iref = 0
				self.vref = 0
				if(voltage < V_BAT_DETECT):
					self.state = 0
					self.capacity = 0

		if voltage > self.vref:
			if(self.cc_cv):
				self.cc_cv = self.cc_cv -1
			else:
				self.cmode = 0
		if current > self.iref:
			self.cmode = 1
			self.cc_cv = self.CURRENT_MODE

		return self.state

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient

class aws_client():

    def __init__(self):
        self.caPath = "./certificates/aws_iot_rootCA"
        self.certPath = "./certificates/70851ab1a8-certificate.pem.crt"
        self.keyPath = "./certificates/70851ab1a8-private.pem.key"

    def connect_aws(self):
        print("Trying to connected to AWS")
        self.myShadowClient = AWSIoTMQTTShadowClient("fog")
        
        self.myShadowClient.configureEndpoint("a244tup59yrtj1.iot.us-west-2.amazonaws.com", 8883)
        
        self.myShadowClient.configureCredentials(self.caPath, self.keyPath, self.certPath)
        
        self.myShadowClient.configureConnectDisconnectTimeout(10)  # 10 sec
        self.myShadowClient.configureMQTTOperationTimeout(5)  # 5 sec

        self.myShadowClient.configureAutoReconnectBackoffTime(1, 32, 20)
        self.myShadowClient.connect()
        print("connected to AWS")
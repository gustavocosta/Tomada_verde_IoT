import paho.mqtt.client as mqtt
from threading import Thread
from thing import thing
from aws_client import aws_client
import time

awsc = aws_client()
mqttc = mqtt.Client()

things_connected = []

def on_connect(mosq, obj, msg, result):
	print("Connected to Mosquitto\n")

def on_disconnect(mosq, obj, msg):
	print("Disonnected from Mosquitto\nTrying to reconnect\n")
	mqtt_connection().connect()

def new_connection(mosq, obj, msg):
	print("trying to register new thing connection")
	try:
		message = msg.payload.split('|')
		if (message[0] == 'new_device') & (msg.payload.count('|') == 1):
			if(things_connected.count(message[1]) == 0):
				print("Thing name: " + message[1])
				new_thing = thing(message[1], mqttc, awsc)
				things_connected.append(message[1])
				print("Thing connection established\n")
			else:
				print("Thing connection already established\n")
			mqttc.publish("connection/" + message[1] + "/status", "accept")
		else:
			print("Incorect Mensage: " + str(msg.payload) + "\n")
	except:
		print("Error in connection/new message")

class mqtt_connection():
	def __init__(self):
		self.ip =  "192.168.0.168"#wifi
		print("Trying to connect to Mosquitto")
		mqttc.on_connect = on_connect
		mqttc.on_disconnect = on_disconnect
		self.connect()
		awsc.connect_aws()

	def connect(self):
		disconnect = 1

		while disconnect:
			try:
				mqttc.connect(self.ip, 1883, 60)
				connected = 1
			except:
				connected = 0
				print('Conection failed')
	
			if(connected):
				disconnect = 0
			else:
				time.sleep(5)


	def connect_subscribe(self):
		mqttc.message_callback_add("connection/new", new_connection)
		mqttc.subscribe("connection/#", 0)
		mqttc.loop_forever()
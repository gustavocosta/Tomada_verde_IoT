import paho.mqtt.client as mqtt
from threading import Thread
import time
import json

from battery_charge import battery_charge
from plug_consumption import plug_consumption
from threading import Thread
from plug_power import plug_power
from aws_client import aws_client
from aws_thing import aws_thing

jpayload = 0

def on_connect(mqttc, obj, flags, rc):
	print("rc: "+str(rc))

def on_message(mqttc, obj, msg):
	print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

def get_callback(payload, responseStatus, token):
	print ("[Get] Payload: " + payload + " | ResponseStatus: " + responseStatus + " | Token: " + token + "\n")
	global jpayload
	jpayload = json.loads(payload)

class thing():

	def __init__(self, tn, mqtt, aws):
		global jpayload
		self.thing_name = tn
		self.awsc = aws_thing(self.thing_name, aws)
		self.pp = plug_power()
		self.bc = battery_charge()
		self.pc = plug_consumption()
		self.mqttc = mqtt
		jpayload = 0

		if(self.awsc.get_thing()):
			self.awsc.get_shadow(get_callback)
			while jpayload == 0:
				pass
			self.power = jpayload['state']['reported']['power']
			self.bat_current = jpayload['state']['reported']['bat_current']
			self.bat_voltage = jpayload['state']['reported']['bat_voltage']
			self.bat_charge = jpayload['state']['reported']['bat_charge']
			self.bat_state = jpayload['state']['reported']['bat_state'] # IDLE = 0, FAULT = 1, DONE = 2, PRECHARGE = 3, CHARGE = 4, FLOAT = 5
			self.con_current = jpayload['state']['reported']['con_current']
			self.con_potency = jpayload['state']['reported']['con_potency']
			self.con_price = jpayload['state']['reported']['con_price']
		else:
			self.power = 0
			self.bat_current = 0
			self.bat_voltage = 0
			self.bat_charge = 0
			self.bat_state = 0 # IDLE = 0, FAULT = 1, DONE = 2, PRECHARGE = 3, CHARGE = 4, FLOAT = 5
			self.con_current = 0
			self.con_potency = 0
			self.con_price = 0
			self.awsc.update_shadow({"device":self.thing_name,"power":self.power,
				"con_current":self.con_current,"con_potency":self.con_potency,"con_price":self.con_price,
				"bat_current":self.bat_current,"bat_voltage":self.bat_voltage,"bat_charge":self.bat_charge,"bat_state":self.bat_state})
                
		t = Thread(target=self.plug_subscribe, args=())
		t.start()
		t = Thread(target=self.check_power, args=())
		t.start()

	def plug_subscribe(self):
		self.mqttc.message_callback_add("plug/" + self.thing_name + "/sensor/#", self.on_plug_sense_msg)
		self.mqttc.message_callback_add("plug/" + self.thing_name + "/sensor/battery", self.on_plug_sensor_battery_msg)
		self.mqttc.message_callback_add("plug/" + self.thing_name + "/sensor/consumption", self.on_plug_sense_consumption_msg)
		self.mqttc.subscribe("plug/" + self.thing_name + "/#", 0)

	def on_plug_sense_msg(self, mosq, obj, msg):
		print("Plug sensors: " + msg.topic + " " + str(msg.payload))

	def on_plug_sensor_battery_msg(self, mosq, obj, msg):
		try:
			self.message = str(msg.payload).split('|')
			self.bat_current = float(self.message[0])
			self.bat_voltage = float(self.message[1])
			self.bat_charge = self.bc.b_charge(self.bat_current)
			self.bat_state = self.bc.b_state(self.bat_current, self.bat_voltage)
			self.awsc.update_shadow({"device":self.thing_name,"power":self.power,
				"con_current":self.con_current,"con_potency":self.con_potency,"con_price":self.con_price,
				"bat_current":self.bat_current,"bat_voltage":self.bat_voltage,"bat_charge":self.bat_charge,"bat_state":self.bat_state})
		except:
			print("Error in plug/" + self.thing_name + "/sensor/battery message")

	def on_plug_sense_consumption_msg(self, mosq, obj, msg):
		try:
			self.con_current = float(msg.payload)
			self.con_potency = self.con_potency + self.pc.calc_consuption(self.con_current)
			self.con_price = self.pc.get_price()
			self.awsc.update_shadow({"device":self.thing_name,"power":self.power,
				"con_current":self.con_current,"con_potency":self.con_potency,"con_price":self.con_price,
				"bat_current":self.bat_current,"bat_voltage":self.bat_voltage,"bat_charge":self.bat_charge,"bat_state":self.bat_state})
		except:
			print("Error in plug/" + self.thing_name + "/sensor/consuption message")

	def check_power(self):
                self.mqttc.publish("plug/" + self.thing_name + "/power", 0)
		while 1:
			time.sleep(30)
			if self.pp.check_hour():
				self.power = self.pp.state
				self.awsc.update_shadow({"device":self.thing_name,"power":self.power,
					"con_current":self.con_current,"con_potency":self.con_potency,"con_price":self.con_price,
					"bat_current":self.bat_current,"bat_voltage":self.bat_voltage,"bat_charge":self.bat_charge,"bat_state":self.bat_state})
			self.mqttc.publish("plug/" + self.thing_name + "/power", self.power)

#####TEST#####
#mqtth = mqtt.Client()
#mqtth.on_connect = on_connect
#mqtth.on_message = on_message
#mqtth.connect("192.168.0.15", 1883, 60)
#mqtth.connect("172.20.8.189", 1883, 60)
#thing_h = thing("cell1", mqtth)
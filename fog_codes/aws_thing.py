import json
import subprocess

def update_callback(payload, responseStatus, token):
	print ("[Update] Payload: " + payload + " | ResponseStatus: " + responseStatus + " | Token: " + token + "\n")

class aws_thing():

	def __init__(self, tn, aws):
		self.thing_name = tn
		self.myDeviceShadow = aws.myShadowClient.createShadowHandlerWithName(self.thing_name, True)

	def run_sh_cmd(self, cmd):
		return subprocess.Popen(cmd, shell=True, stdout= subprocess.PIPE)

	def get_thing(self):
		print("check if exist a thing with this name in AWS IoT")
		ret = self.run_sh_cmd("aws iot describe-thing --thing-name \"" + self.thing_name + "\"")
		#if not exist, then create a thing
		if (ret.wait() == 255):
			print("trying to create a new thing on aws")
			ret = self.run_sh_cmd("aws iot create-thing --thing-name \"" + self.thing_name + "\"")
			self.attach_certificate()
			print("thing created\n")
			exist = False
		else:
			print("thing found\n")
			exist = True
		return exist

	def attach_certificate(self):
		print("get certificate ARN\n")
		ret = self.run_sh_cmd("aws iot list-certificates")
		if (ret.wait() == 0):
			certificateARN = json.loads(ret.stdout.read())["certificates"][0]["certificateArn"]
			ret = self.run_sh_cmd("aws iot attach-thing-principal --thing-name \"" + self.thing_name + "\" --principal \"" + certificateARN + "\"")
			if (ret.wait() != 0):
				print("[Error]")
		else:
			print("[Error]")

	def get_shadow(self, callback):
		return self.myDeviceShadow.shadowGet(callback, 5)

	def update_shadow(self, shadow_state):
		payload = {"state":{"reported":shadow_state}}
		self.myDeviceShadow.shadowUpdate(json.dumps(payload), update_callback, 5)
class plug_consumption():
	def __init__(self):
		self.consuption = 0 #Wh

	def calc_consuption(self, current):
		self.consuption = current*210*0.0166
		return (self.consuption/1000) #kWh

	def get_price(self):
		return (self.consuption/1000)*0.46 #R$
		
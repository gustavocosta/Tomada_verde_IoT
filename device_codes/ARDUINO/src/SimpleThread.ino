#include "Thread.h"
#include "ThreadController.h"
#include <TimerOne.h>
#include "EmonLib.h"
#include <SoftwareSerial.h>



 #define WINDOW 10 //30 ssegundos de amostras para retirar o ruído
 #define TEMPO 60 // Uma hora de amostras

//VARIÁVEIS DO SENSOR DE CORRENTE HALL
 int contador =0;
 double sensorHallValue=0.00000;
 double vIrms[WINDOW];
 double cIrms = 0;
 double delta=0.00000;



EnergyMonitor emon1;

class VoltageSensor: public Thread{
public:
    //VARIÁVEIS DO SENSOR DE TENSÃO
    double vout = 0.0;
    int pinVoltage;
    double vin = 0.0;
    double R1 = 30000.0; //
    double R2 = 7500.0; //
    int value = 0;
    void run(){
    value = analogRead(pinVoltage);
    vout = (value * 5.0) / 1024.0; // see text
    vin = vout / (R2/(R1+R2));

    runned();
    }
};

/*class MonitoringSerial: public Thread{
public:



    runned();
    }
};*/

class CurrentSensor: public Thread{
public:
  double Irms;
    void run(){
      Irms = emon1.calcIrms(1480)-delta;
      //Serial.println(Irms);
      if(Irms <0.03){
        Irms=0.00000;
      }
    Serial.println(Irms);
      runned();
    }
};

VoltageSensor voltage = VoltageSensor();

CurrentSensor sensorHall = CurrentSensor();

ThreadController controller = ThreadController();

void timerCallback(){
	controller.run();
}

void setup(){

	 Serial.begin(115200);


    voltage.pinVoltage = A4;
    voltage.setInterval(100);

     emon1.current(0, 20);
     for (int i=0; i<WINDOW; i++) {
           vIrms[i] = emon1.calcIrms(1480);
           cIrms = cIrms + vIrms[i];
           delay(100);
        }
         delta = cIrms/(WINDOW);
         Serial.print("Delta ");
         Serial.println(delta);


         sensorHall.setInterval(500);

	 controller.add(&sensorHall);
   controller.add(&voltage);



	 Timer1.initialize(20000);
	 Timer1.attachInterrupt(timerCallback);
	 Timer1.start();

}

void loop(){
  //unsigned long timethen = millis();


  // Serial.print("Sensor de Corrente: ");
	// Serial.println(sensorHall.Irms);
  // Serial.print("Sensor de Tensão da Bateria: ");
  Serial.print("voltage: ");
  Serial.println(voltage.vin);


  //unsigned long atraso = millis() - timethen;
  //Serial.println(atraso);
  delay(1000);

}

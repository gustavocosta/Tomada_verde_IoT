from django.conf.urls import url

from . import views

app_name = 'main'
urlpatterns = [
	url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<thing_id>[0-9]+)/$', views.detailView, name='detail'),
]
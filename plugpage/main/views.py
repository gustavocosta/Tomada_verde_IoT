from django.views import generic
from django.shortcuts import get_object_or_404, render
from graphos.sources.model import ModelDataSource
from graphos.renderers import flot
from graphos.sources.simple import SimpleDataSource
from graphos.renderers.gchart import AreaChart

import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

from main.models import Thing

class IndexView(generic.ListView):
	template_name = 'main/index.html'
	context_object_name = 'things_list'
	def get_queryset(self):
		try:
			Thing.objects.all().delete()
			client = boto3.client('iot')
			request = client.list_things()
			things_list = request['things']
			for thing in things_list:
				t = Thing(thing_name=thing['thingName'])
				t.save()
		except:
			print('Error on get things names')

		return Thing.objects.all()

def detailView(request, thing_id):
	thing = get_object_or_404(Thing, pk=thing_id)

	con_val = 0
	bat_val = 0
	data = [
		['dia', 'kWh/dia']
	]

	try:
		dynamodb = boto3.resource("dynamodb")
		table = dynamodb.Table('PlugTable')
		response = table.scan(FilterExpression=Key("device").eq(thing.thing_name))
	except ClientError:
		print('Error on get thing informations')
	else:
		bubbleSort(response['Items'])

		#Limits the number of days to 7
		if(len(response['Items']) > 7):
			length = 7
		else:
			length = len(response['Items'])
			#Append potency 0 for the subtraction
			response['Items'].append({'con_potency': 0})

		con_val = round(response['Items'][0]['con_price'], 2)
		bat_val = round(response['Items'][0]['bat_charge'], 1)

		#Apend from the oldest to the most recent
		i = 1
		while i <= length:
			data.append([response['Items'][length-i]['date'],
				response['Items'][length-i]['con_potency']-response['Items'][length-i+1]['con_potency']])
			i = i+1

	chart = AreaChart(SimpleDataSource(data=data), options={'title': "Consumo"})
	context = {'chart': chart, 'con_val': con_val, 'bat_val': bat_val}
	return render(request, 'main/detail.html', context)

#Order from the most recent to the oldest
def bubbleSort(alist):
	for passnum in range(len(alist)-1,0,-1):
		for i in range(passnum):
			if alist[i]['time']<alist[i+1]['time']:
				temp = alist[i]
				alist[i] = alist[i+1]
				alist[i+1] = temp
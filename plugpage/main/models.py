from django.db import models
from django.utils.encoding import python_2_unicode_compatible

class Thing(models.Model):
	thing_name = models.CharField(max_length=200)

	def __str__(self):
		return self.thing_name
